name := "AkkaWithStorm"

organization := "AkkaWithStorm"

version := "1.0"

scalaVersion := "2.10.3"

libraryDependencies ++= Seq("com.github.velvia" % "scala-storm_2.10" % "0.2.3-2.10-SNAPSHOT",
	"org.apache.storm" % "storm-core" % "0.9.1-incubating" % "provided",
	"com.typesafe.akka" %% "akka-actor"   % "2.2.4"
)

resolvers ++= Seq("clojars" at "http://clojars.org/repo/",
	"clojure-releases" at "http://build.clojure.org/releases",
	"moma" at "https://github.com/prassee/moma/raw/master/snapshots")