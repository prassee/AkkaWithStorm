package scrawl

import akka.actor.ActorSystem

import akka.actor.Props
object TCPServerActorApp extends App {

  def startTCP() {
    println("exposing new TCP port for message queue @ 6789")
    val server = ActorSystem().actorOf(Props(new TCPServerActor(6789)))
    val fileSrc = scala.io.Source.fromFile(this.getClass().getResource("/gitsActorRead.csv").getPath())
    fileSrc.getLines().foreach(line => {
      server.!(ParsedLineData(line))
    })
  }

  startTCP()
}