package scrawl

import backtype.storm.LocalCluster
import backtype.storm.topology.TopologyBuilder
import backtype.storm.Config
import scala.io.Source

object StormJumpStart extends App {
  val dsl = Source.fromFile(this.getClass().getResource("/blenderDSL.txt").getPath())

  val fieldMappings = Map("name" -> 0, "empId" -> 1, "caseId" -> 2, "caseType" -> 3,
    "createdDate" -> 4, "updatedDate" -> 5)

  val platform = BlenderStormPlatform(dsl.mkString)
  platform.buildFrom(fieldMappings)
  platform.kickStorm()

  val tp = new TopologyBuilder
  tp.setSpout("MessageReadingSpout", new AkkaQueueSpout())
  tp.setBolt("LineProcessingBolt", new LineProcessingBolt())
  val lc = new LocalCluster
  val conf = new Config
  lc.submitTopology("AkkaBasedStormTopology", conf, tp.createTopology())
}

