package scrawl

import java.net.InetSocketAddress

import scala.collection.mutable.Queue

import akka.actor._
import akka.actor.Actor
import akka.actor.IO
import akka.actor.IOManager
import akka.actor.actorRef2Scala
import akka.util.ByteString

case class ParsedLineData(str: String)

class TCPServerActor(port: Int) extends Actor {

  val queue = new Queue[String]

  override def preStart {
    IOManager(context.system) listen new InetSocketAddress(port)
  }

  def receive = {
    case ParsedLineData(str) => queue.enqueue(str)
    case IO.NewClient(server) => server.accept()
    case IO.Read(rHandle, bytes) =>
      rHandle.asSocket.write(ByteString(queue.dequeue() + "\n".getBytes()).compact)
  }
}

