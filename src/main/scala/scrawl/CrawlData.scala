package scrawl

import backtype.storm.tuple.Tuple
import storm.scala.dsl.StormBolt
import storm.scala.dsl.StormSpout
import java.io.BufferedReader
import java.net.Socket
import java.io.DataOutputStream
import java.io.InputStreamReader
import scala.io.Source
import java.io.PrintWriter
import java.io.File
import java.io.FileWriter
import java.io.BufferedWriter

/**
 * spout which reads messages off from a TCPQueue
 */
class AkkaQueueSpout()
  extends StormSpout(outputFields = List("Line")) {

  def nextTuple {
    val next = nextTupleImpl
    emit(next)
  }

  def nextTupleImpl = {
    val socket = new Socket("localhost", 6789)
    val br = new BufferedReader(new InputStreamReader(socket.getInputStream()))
    val dos = new DataOutputStream(socket.getOutputStream())
    dos.writeChars("\n")
    val x = br.readLine()
    dos.close()
    br.close()
    x
  }
}

class LineProcessingBolt()
  extends StormBolt(outputFields = List("BlenderDefaultBolt")) {

  def execute(t: Tuple) {
    println(t.getValue(0).toString())
  }
}

